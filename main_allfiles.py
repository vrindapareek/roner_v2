import json
import os.path
import glob
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from pandas.io.json import json_normalize
from nltk.tokenize import word_tokenize
from sklearn import preprocessing
import re
from nltk.util import ngrams
from nltk.corpus import stopwords
pd.set_option('max_columns', 22)



dftarget = pd.read_csv('BRO.csv')
print(dftarget.head())
dftarget = dftarget[['Preferred Label', 'Synonyms']]
# print(dftarget)
print("\n")
target_list = (dftarget[['Preferred Label', 'Synonyms']].values.tolist())
#flatten list
target_list = [item for sublist in target_list for item in sublist]
target_list = [target_list for target_list in target_list if str(target_list) != 'nan']

count=1
for name in glob.glob('inputdata/*'):
    print(name)
    with open(name, encoding="utf8") as file:
        input_data = json.load(file)
    dfinput = pd.DataFrame.from_dict(json_normalize(input_data), orient='columns')

    #print(dfinput.T)
    dfinput = dfinput.astype(str)
    dfinput['target_coli'] = dfinput.apply(lambda x: ' '.join(x), axis = 1)
    dfinput['target_coli'] = dfinput['target_coli'].str.replace('[^\w\s]','')
    dfinput['target_coli'] = dfinput['target_coli'].str.lower()

    #print(dfinput)
    ls = dfinput['target_coli'].tolist()
    res = ("".join(map(str, ls)))
    tokens = word_tokenize(res)
    text = [tokens for tokens in tokens if str(tokens) != 'nan']
    print('\n')
    input_str = (' '.join(map(str, text)))
    # print(input_str)

    #ngrams
    def generate_ngrams(input_str, n):
        s = input_str.lower()
        s = re.sub(r'[^a-zA-Z0-9\s]', ' ', s)
        tokens = [token for token in s.split(" ") if token != ""]
        ngrams = zip(*[tokens[i:] for i in range(n)])
        return [" ".join(ngram).lower() for ngram in ngrams]

    onegram = generate_ngrams(input_str, 1)
    twogram = generate_ngrams(input_str, 2)
    threegram = generate_ngrams(input_str, 3)
    fourgram = generate_ngrams(input_str, 4)

    allgrams = onegram + twogram + threegram + fourgram


    def cos_similarity(ngram):

        tfidf_vectorizer = TfidfVectorizer()
        tfidf_matrix_matched = tfidf_vectorizer.fit_transform(ngram)
        similarity_matrix = cosine_similarity(tfidf_matrix_matched[0:1], tfidf_matrix_matched)
        score_list = [item for sublist in similarity_matrix.tolist() for item in sublist]
        # print(score_list)
        return score_list


    #any score
    similarity_list = []
    a  = 0
    for i in target_list:
        allgrams.insert(0,i)
        similarity_score = cos_similarity(allgrams)
        similarity_score = list(filter((0.0).__ne__, similarity_score))
        similarity_score = sorted(similarity_score, reverse= True)
        similarity_list.append(similarity_score[1:2])
        allgrams.pop(0)
        a += 1

    similarity_df = pd.DataFrame({'Target': target_list, 'Similarity_Score': similarity_list })
    similarity_df = (similarity_df[similarity_df['Similarity_Score'].str.len() != 0])
    similarity_df.loc[:, 'Similarity_Score'] = similarity_df.Similarity_Score.map(lambda x: x[0])
    print(similarity_df)
    similarity_df['Similarity_Score'] = pd.Series([float("{0:.2f}".format(val * 100)) for val in similarity_df['Similarity_Score']], index = similarity_df.index)
    similarity_df.loc[similarity_df.Similarity_Score >= float(40.00), 'Score_Threshold'] = 'Accepted'
    similarity_df = similarity_df.sort_values(['Similarity_Score'], ascending = False)
    print('\nDataframe with similarity score of all tags:\n-------------------------------------------- \n', similarity_df)
    # similarity_df.to_html('similarity_score.html')
    threshold_df= similarity_df.dropna()
    threshold_df = threshold_df.sort_values(['Similarity_Score'], ascending = False)
    print('\nDataframe with similarity score of tags with a match above 40%:\n-------------------------------------------------------------- \n', threshold_df)
    # threshold_df.to_html('threshold_match.html')

    count_values = len(threshold_df)
    if not threshold_df.empty:
        # count_values = len(threshold_df)
        print('\n')

        tag_list = list(set(threshold_df["Target"]))

        #putting tags
        new_dict = {}
        # print(type(tag_dict))
        with open(name, encoding="utf8") as file:
            data = json.load(file)

        for i in data:
            new_dict.update(i)
        #print(new_dict)

        new_dict["tags"] = tag_list
            #print(new_dict)
        annotated_json = json.dumps(new_dict, indent=4)


        #naming output file
        filename = name[name.find("-") + 1:name.find(".")]
        filename = " ".join(re.findall("[A-Z]+", filename))
        print(filename)



        filepath = 'D:/CSC-791 Computational Bioscience/RONER_0422/outputdata/'
        output_file = str(count)+filename+".json"
        outfile = os.path.join(filepath, output_file)
        with open(outfile, 'w') as file:

            json.dump(new_dict, file, indent=4)
        #print(annotated_json)

        with open(outfile) as f:
            outdata = json.load(f)
        dftarget = pd.DataFrame.from_dict(json_normalize(outdata),  orient='columns')
        #print(dftarget.T)


        print('\n')
        print('total count:', count_values)
        count +=1


    else:
        filename = name[name.find("-") + 1:name.find(".")]
        filename = " ".join(re.findall("[A-Z]+", filename))
        print(filename)

        filepath = 'D:/CSC-791 Computational Bioscience/RONER_0422/outputdata/'
        output_file = str(count)+filename+".json"
        outfile = os.path.join(filepath, output_file)


        with open (outfile, 'w', encoding="utf8") as f:
            with open(name, 'r+', encoding="utf8") as file:
                data = json.load(file)
                f.write(json.dumps(data, indent=4))
        count += 1

